variable "cloud_provider" {
    type = string
    description = "The cloud provider to use. This must be 'ovh'."
    default = "ovh"

    validation {
        condition = contains(["ovh"], var.cloud_provider)
        error_message = "Cloud_provider must be 'ovh'."
    }
}
variable "deployment" {
    type = string
    description = "The deployment tag. Will be added as metadata to the runner instance"
}
variable "env_tag" {
    type = string
    description = "The environment tag. Will be added as metadata to the runner instance"
}
variable "gitlab_runner_name" {
    type = string
    description = "The Gitlab Runner name"
}
variable "gitlab_runner_registration_token" {
    type = string
    description = "The registration token"
}
variable "gitlab_runner_api_token" {
    type = string
    description = "An API token which has permissions on the project associated with the runner"
}
variable "gitlab_runner_description" {
    type = string
    description = "The description for the Gitlab Runner"
}
variable "gitlab_runner_limit" {
    default = "4"
    description = "The number of jobs which can run concurrently on this runner "
}
variable "gitlab_runner_concurrent_jobs" {
    default = "4"
    description = "The number of jobs which can run on the system at once"
}
variable "gitlab_runner_url" {
    type = string
    description = "The URL of the gitlab instance to connect to"
}
variable "default_user" {
    default = "centos"
    description = "The default user to SSH into the instance with"
}
