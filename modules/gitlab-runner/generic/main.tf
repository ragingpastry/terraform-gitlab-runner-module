
module "gitlab-runner" {
    source = "../ovh"
    count = var.cloud_provider == "ovh" ? 1 : 0
    deployment = var.deployment
    env_tag = var.env_tag
    gitlab_runner_name = var.gitlab_runner_name
    gitlab_runner_registration_token = var.gitlab_runner_registration_token
    gitlab_runner_api_token = var.gitlab_runner_api_token
    gitlab_runner_description = var.gitlab_runner_description
    gitlab_runner_limit = var.gitlab_runner_limit
    gitlab_runner_concurrent_jobs = var.gitlab_runner_concurrent_jobs
    gitlab_runner_url = var.gitlab_runner_url
}
