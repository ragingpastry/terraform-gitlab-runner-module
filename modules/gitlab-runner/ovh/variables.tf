variable "deployment" {}
variable "env_tag" {}
variable "gitlab_runner_name" {}
variable "gitlab_runner_registration_token" {}
variable "gitlab_runner_api_token" {}
variable "gitlab_runner_description" {}
variable "gitlab_runner_limit" {}
variable "gitlab_runner_concurrent_jobs" {}
variable "gitlab_runner_url" {}
variable "default_user" {
    default = "centos"
}
