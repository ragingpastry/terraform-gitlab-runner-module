data "openstack_images_image_v2" "centos" {
    name = "Centos 7"
    most_recent = true
}

data "openstack_compute_flavor_v2" "small" {
    name = "s1-2"
}

resource "openstack_compute_keypair_v2" "gitlab_runner_keypair" {
  name = "${var.gitlab_runner_name}-keypair"
}

resource "openstack_compute_instance_v2" "gitlab_runner" {
  name            = var.gitlab_runner_name
  image_id        = data.openstack_images_image_v2.centos.id
  flavor_id       = data.openstack_compute_flavor_v2.small.id
  key_pair        = openstack_compute_keypair_v2.gitlab_runner_keypair.name
  security_groups = ["default"]

  metadata = {
    Environment = var.env_tag
    Deployment = var.deployment
    Node = "runner"
    Name = "IaC Deploy ${var.deployment}"
  }

  network {
    name = "Ext-Net"
  }
}

resource "null_resource" "DeployGitlabRunner" {
  provisioner "local-exec" {
    command = "ansible-galaxy install -r ${path.module}/../../../ansible/requirements.yml"
  }
  provisioner "local-exec" {
    command = "echo \"${openstack_compute_keypair_v2.gitlab_runner_keypair.private_key}\" >> /tmp/key"
  }
  provisioner "local-exec" {
    command = "chmod 0600 /tmp/key"
  }
  provisioner "local-exec" {
    command = <<EOT
      ansible-playbook -u ${var.default_user} -i '${openstack_compute_instance_v2.gitlab_runner.access_ip_v4},' \
      --private-key /tmp/key --become ${path.module}/../../../ansible/deploy_gitlab_runner.yml
    EOT

    environment = {
      GITLAB_RUNNER_NAME = var.gitlab_runner_name
      GITLAB_RUNNER_REGISTRATION_TOKEN = var.gitlab_runner_registration_token
      GITLAB_RUNNER_API_TOKEN = var.gitlab_runner_api_token
      GITLAB_RUNNER_DESCRIPTION = var.gitlab_runner_description
      GITLAB_RUNNER_LIMIT = var.gitlab_runner_limit
      GITLAB_RUNNER_CONCURRENT_JOBS = var.gitlab_runner_concurrent_jobs
      GITLAB_RUNNER_URL = var.gitlab_runner_url
      ANSIBLE_HOST_KEY_CHECKING = "False"
    }
  }
}
